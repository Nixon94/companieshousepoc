﻿using CompaniesHouse;
using CompaniesHouse.Request;
using CompaniesHouse.Response.Appointments;
using CompaniesHouse.Response.Officers;
using CompaniesHouse.Response.Search.OfficerSearch;
using System.Globalization;
using Officer = CompaniesHouse.Response.Search.OfficerSearch.Officer;

namespace CompaniesHousePoc;

public class Program
{
    private static async Task Main()
    {
        var apiKey = "c354b1f7-7097-406c-8ff3-2c7bf2a04155";
        using var client = CreateCompaniesHouseClient(apiKey);

        //Different order of names and surname gives different number of results!
        //The order returned from Companies House API: FirstName + MiddleNames + LastName/Surname (in all capital letters!), eg. Jacqueline Lisa HALLUMS
        //No matter the order "Jacqueline Lisa HALLUMS" is always first result thus CH probably orders results by the most accurate
        var request = new Request()
        {
            FirstName = "Jacqueline",
            MiddleName = "Lisa",
            LastName = "Hallums",
            //Will we have client's date of birth at this stage?
            DateOfBirth = DateTime.ParseExact("1965-08-27", "yyyy-MM-dd", CultureInfo.InvariantCulture)
        };

        var firstName = request.FirstName;
        var middleName = request.MiddleName;
        var lastName = request.LastName.ToUpper();
        var nameToSearchFor = $"{firstName} {middleName} {lastName}";

        var yearOfBirth = request.DateOfBirth.Year;
        var monthOfBirth = request.DateOfBirth.Month;

        var searchOfficerRequest = new SearchOfficerRequest()
        {
            Query = nameToSearchFor,
            StartIndex = 0,
            ItemsPerPage = 100
        };

        var officerSearch = await client.SearchOfficerAsync(searchOfficerRequest);

        //Only physical persons have date_of_birth property
        var physicalOfficers = officerSearch.Data.Officers.Where(x => x.DateOfBirth != null);

        //Title is a person full name, this check will need to be improved
        var officerMatches = physicalOfficers.Where(x =>
            x.Title.Contains(firstName) &&
            x.Title.Contains(middleName) &&
            x.Title.Contains(lastName) &&
            x.DateOfBirth.Year == yearOfBirth &&
            x.DateOfBirth.Month == monthOfBirth)
            .ToList();

        if (officerMatches.Any())
        {
            //What should happen if we will find more than one match with exact same name and dob? Return Enum as a result, eg. IsDirector, MultipleMatchesFound, NoMatchesFound?
            var exactPersonAppointments = await client.GetAppointmentsAsync(officerMatches.First().OfficerId);

            var isDirector = exactPersonAppointments.Data.Items.Any(IsDirector);

            //At the appointment level we have a separate properties for first name, surname, etc. which might be helpful in identifying if that's the person we're looking for
            var firstAppointment = exactPersonAppointments.Data.Items.First();
            var forenameMatch = firstAppointment.NameElements.Forename.Equals(firstName);
            var otherForenamesMatch = firstAppointment.NameElements.OtherForenames.Equals(middleName);
            var surnameMatch = firstAppointment.NameElements.Surname.Equals(lastName);

            DisplayOfficersResults(officerSearch, nameToSearchFor);
        }
        else
        {
            Console.WriteLine($"No {nameToSearchFor} director found born {request.DateOfBirth}.");
        }
    }

    private static CompaniesHouseClient CreateCompaniesHouseClient(string apiKey)
    {
        var settings = new CompaniesHouseSettings(apiKey);
        return new CompaniesHouseClient(settings);
    }

    private static bool IsDirector(Appointment appointment)
    {
        //Are those all the roles we should check for?
        List<OfficerRole> directorRoles = new List<OfficerRole>()
        {
            OfficerRole.Director,
            OfficerRole.CorporateDirector,
            OfficerRole.NomineeDirector,
            OfficerRole.CorporateNomineeDirector
        };

        //If resignation date is null it means that the person is still a director in this company
        return directorRoles.Contains(appointment.OfficerRole) && appointment.ResignedOn == null;
    }

    private static void DisplayOfficersResults(CompaniesHouseClientResponse<OfficerSearch> result, string nameSearchedFor)
    {
        //Show all Officers found
        Console.WriteLine($"{Environment.NewLine}----------------------------------------------");
        Console.WriteLine($"Officers found when searching for '{nameSearchedFor}' :");
        foreach (Officer item in result.Data.Officers)
        {
            var link = item.Links.Self;
            Console.WriteLine($"* {item.Title} - {item.Description}");
        }
    }
}